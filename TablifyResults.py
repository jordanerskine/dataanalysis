import sys, csv, os, json, matplotlib.pyplot as plt, numpy as np, re
import matplotlib
from matplotlib import cm
from copy import deepcopy as copy
matplotlib.use('TKAgg')

class DataPoint:
    def __init__(self,value,timestep):
        self.value = value
        self.timestep = timestep

class Run:
    def __init__(self,variant, name):
        self.variant = variant
        self.name = name
        self.data = []

def arg(tag, default):
    if tag in sys.argv:
        val = sys.argv[sys.argv.index(tag)+1]
    else:
        val = default
    return val

def smoothData(data):
    global smoothingFactor
    newData = [data[0]]
    for dp in data[1:]:
        newData.append(newData[-1]*smoothingFactor+(1-smoothingFactor)*dp)
    return newData

def getKeyVariables(variants):
    keys = compareDicts(variants)
    newKeys = []
    for key in keys:
        if isinstance(variants[0][key],dict):
            freshKeys = compareDicts([v[key] for v in variants])
            for k in freshKeys:
                newKeys.append("{}:{}".format(key,k))
        else:
            newKeys.append(key)
    return newKeys

def compareDicts(dicts):
    keys = []
    for d in dicts:
        for key in d:
            for dd in dicts:
                if not (key in dd and dd[key] == d[key]):
                    if key not in keys:
                        keys.append(key)
    return keys

def createDict(table,tags,data):
    tag = tags[0]
    if tag not in table:
        if len(tags) == 1:
            table[tag] = [data]
        else:
            table[tag] = {}
            table[tag] = createDict(table[tag],tags[1:],data)

    else:
        if len(tags) == 1:
            table[tag].append(data)
        else:
            table[tag] = createDict(table[tag],tags[1:],data)
    
    return table


def analyseData(dataTable):
    resultsTable = {}
    for tag in dataTable:
        if isinstance(dataTable[tag],dict):
            resultsTable[tag] = analyseData(dataTable[tag])
        else:
            data = dataTable[tag][0]
            dataList = []
            for run in data:
                x = [d.value for d in run.data]
                dataList.append(np.mean(x[-10:]))
                # dataList.append(run.data[-1].timestep)
            resultsTable[tag] = np.mean(dataList)
    return resultsTable

def printResults(results,startStr = ""):
    for name in results:
        if isinstance(results[name],dict):
            print("{}{}:".format(startStr,name))
            printResults(results[name],startStr=startStr+"    ")
        else:
            print("{}{}:{}".format(startStr,name,results[name]))
        


path = arg("--path","./Data/2RoomSweep")
var = arg("--var",None)
showStd = arg("--showStd",True)
ignoreKeys = re.split(",",arg("--ignoreKeys",""))
hideLabelKeys = re.split(",",arg("--hideLabelKeys",""))
includeMissing = arg("--includeMissing",False)
showPlane = int(arg("--showPlane",0))
title = arg("--title",None)
ylabel = arg("--ylabel",None)
showAll = arg("--showAll",0)
smoothingFactor = float(arg("--smoothFactor",0.95))
sort = re.split(",",arg("--sort",""))
print(var)

folder = os.listdir(path)

if var is None:
    with open("{}/{}/progress.csv".format(path,folder[0]),'r',newline='') as f:
        reader = list(csv.reader(f, delimiter=','))
        for elem in reader[0]:
            print(elem)
else:
    data = []
    tagPresent = False
    for run in folder:
        try:
            with open("{}/{}/progress.csv".format(path,run),'r',newline='') as f:
                reader = list(csv.reader(f, delimiter=','))
                epochTag = reader[0].index('Epoch')
                if var in reader[0]:
                    tagPresent = True
                    varTag = reader[0].index(var)
                    with open("{}/{}/variant.json".format(path,run),'r') as j:
                        variant = json.load(j)
                        data.append(Run(variant, run))
                    rows = copy(reader[1:])
                    for n,row in enumerate(rows):
                        try:
                            val = float(row[varTag])
                            t = float(row[epochTag])
                            data[-1].data.append(DataPoint(val,t))
                        except:
                            print("{}: Row {} failed".format(run,n))
        except:
            print("{} failed to read".format(run))
            print("Last Time: {}".format(t))
    if not tagPresent:
        print("Bad variable")

    orderedData = {}
    variants = []
    keys = getKeyVariables([run.variant for run in data])
    for run in data:

        name = ""
        for key in keys:
            ks = re.split(":",key)
            if not(ignoreKeys is not None and ks[-1] in ignoreKeys):
                if len(ks)>1:
                    try:
                        name += "{}:{}-".format(ks[1],run.variant[ks[0]][ks[1]])
                    except:
                        pass
                else:
                    name += "{}:{}-".format(key,run.variant[key])
        name = name[:-1]
        if name not in orderedData:
            orderedData[name] = [run]
        else:
            orderedData[name].append(run)

    # for group in sorted(orderedData):
    #     plot = True
    #     for sorting in sort:
    #         tag = re.split(":",sorting)
    #         if len(re.findall(sorting,group)) == 0 and not (includeMissing and len(re.findall(tag[0],group)) == 0):
    #             plot = False
    #     if plot:
    table = dict()
    for group in sorted(orderedData):
        tags = re.split('-',group)
        table = createDict(table,tags,orderedData[group])
    results = analyseData(table)
    
    printResults(results)

    cmap = plt.get_cmap('jet')
    
    # for name in results:
    #     x = [float(re.split(":",d)[1]) for d in results[name]['mode:cooperative']]
    #     y = [results[name]['mode:cooperative'][d] for d in results[name]['mode:cooperative']]
    #     plt.plot(x,y,label=name)

    if not showPlane:
        increm = int(256/11)
        colour = 0
        for name in results:
            x = [float(d) for d in results[name]]
            y = [results[name][d] for d in results[name]]
            plt.plot(x,y,label=name,color=cmap(colour))
            colour += increm
        
        plt.title("Success rate by individual cooperative ratio")
        plt.ylabel("Success rate")
        plt.xlabel("Cooperative ratio for policy 2")
        plt.grid()
        plt.legend()
        plt.show()
    
    else:
        fig = plt.figure()
        ax = fig.gca(projection="3d")

        xx,yy = np.meshgrid(range(11),range(11))
        xx = xx/10
        yy = yy/10
        z = np.array([[results[x][y] for y in results[x]] for x in results])
        ax.plot_surface(xx,yy,z,alpha=1,cmap=cm.coolwarm)
        ax.set_xlabel("Policy 2 cooperative ratio")
        ax.set_ylabel("Policy 1 cooperative ratio")
        ax.set_zlabel("Success rate")
        plt.title("Success rate by individual cooperative ratio")
        plt.show()
