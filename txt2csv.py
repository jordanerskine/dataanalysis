import csv, os, sys, re


class txt2csv:
    def __init__(self,fileName):
        with open(fileName+'.txt','r') as f:
            data = f.read()
        
        with open(fileName+'.csv','w') as f:
            writer = csv.writer(f)
            lines = re.split('\n',data)
            for line in lines:
                d = re.split('\t',line)
                writer.writerow(d)



if __name__ == "__main__":
    if '--file' in sys.argv:
        txt2csv(sys.argv[sys.argv.index('--file')+1])
    else:
        print("-----\nConvert a .txt file of the correct format into a .csv\n\ntxt2csv --file FILENAME\n")