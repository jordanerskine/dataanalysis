import matplotlib
gui_env = ['WXAgg','GTKAgg','Qt4Agg','TKAgg']
for gui in gui_env:
    try:
        print("testing" + gui)
        matplotlib.use(gui,warn=False, force=True)
        from matplotlib import pyplot as plt
        break
    except:
        continue
print("Using:",matplotlib.get_backend())