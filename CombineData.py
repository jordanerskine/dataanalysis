import sys, csv, os, json, matplotlib.pyplot as plt, numpy as np, re
import matplotlib
matplotlib.use('TKAgg')

class DataPoint:
    def __init__(self,value,timestep):
        self.value = value
        self.timestep = timestep

class Run:
    def __init__(self,variant, name):
        self.variant = variant
        self.name = name
        self.data = []

def arg(tag, default):
    if tag in sys.argv:
        val = sys.argv[sys.argv.index(tag)+1]
    else:
        val = default
    return val

def smoothData(data):
    global smoothingFactor
    newData = [data[0]]
    for dp in data[1:]:
        newData.append(newData[-1]*smoothingFactor+(1-smoothingFactor)*dp)
    return newData

def getKeyVariables(variants):
    keys = compareDicts(variants)
    newKeys = []
    for key in keys:
        if isinstance(variants[0][key],dict):
            freshKeys = compareDicts([v[key] for v in variants])
            for k in freshKeys:
                newKeys.append("{}:{}".format(key,k))
        else:
            newKeys.append(key)
    return newKeys

def compareDicts(dicts):
    keys = []
    for d in dicts:
        for key in d:
            for dd in dicts:
                if not (key in dd and dd[key] == d[key]):
                    if key not in keys:
                        keys.append(key)
    return keys

def replaceTerms(string):
        replStrs = [['mode',''],
                    [':cooperative','CSAC (ours)'],
                    [':singleAgent','SAC'],
                    [':uncooperative','naive SAC']]
        for inp,out in replStrs:
            string = re.sub(inp,out,string)

        return string

path = arg("--path","./Data/2RoomSweep")
var = arg("--var",None)
showStd = arg("--showStd",True)
ignoreKeys = re.split(",",arg("--ignoreKeys",""))
hideLabelKeys = re.split(",",arg("--hideLabelKeys",""))
includeMissing = arg("--includeMissing",False)
title = arg("--title",None)
ylabel = arg("--ylabel",None)
showAll = arg("--showAll",0)
smoothingFactor = float(arg("--smoothFactor",0.95))
sort = re.split(",",arg("--sort",""))
print(var)

folder = os.listdir(path)
# print("{}/{}/progress.csv".format(path,folder[0]))


if var is None:
    with open("{}/{}/progress.csv".format(path,folder[0]),'r',newline='') as f:
        reader = list(csv.reader(f, delimiter=','))[0]
        for elem in reader:
            print(elem)

else:
    data = []
    tagPresent = False
    for run in folder:
        t = 0
        try:
            with open("{}/{}/progress.csv".format(path,run),'r',newline='') as f:
                reader = list(csv.reader(f, delimiter=','))
                epochTag = reader[0].index('Epoch')
                if var in reader[0]:
                    tagPresent = True
                    varTag = reader[0].index(var)
                    with open("{}/{}/variant.json".format(path,run),'r') as j:
                        variant = json.load(j)
                        data.append(Run(variant, run))
                    for row in reader[1:]:
                        val = float(row[varTag])
                        if val < -10000:
                            val
                        t = float(row[epochTag])
                        data[-1].data.append(DataPoint(val,t))
        except:
            print("{} failed to read".format(run))
            print("Last time: {}".format(t))
    if not tagPresent:
        print("Bad variable")
        print(reader[0])
    
    orderedData = {}
    variants = []
    keys = getKeyVariables([run.variant for run in data])
    for run in data:

        name = ""
        for key in keys:
            ks = re.split(":",key)
            if not(ignoreKeys is not None and ks[-1] in ignoreKeys):
                if len(ks)>1:
                    try:
                        name += "{}:{}-".format(ks[1],run.variant[ks[0]][ks[1]])
                    except:
                        pass
                else:
                    name += "{}:{}-".format(key,run.variant[key])
        name = name[:-1]
        if 'uncooperative' in name:
            name = re.sub('-cooperativeRatio:\d.\d','',name)
        if name not in orderedData:
            orderedData[name] = [run]
        else:
            orderedData[name].append(run)

    cmap = plt.get_cmap('jet')
    if showAll:
        
        for group in sorted(orderedData):
            plot = True
            for sorting in sort:
                tag = re.split(":",sorting)
                if len(re.findall(sorting,group)) == 0 and not (includeMissing and len(re.findall(tag[0],group)) == 0):
                    plot = False
            if plot:
                for run in orderedData[group]:
                    x = [d.timestep for d in run.data]
                    y = [d.value for d in run.data]
                    y = smoothData(y)
                    plt.plot(x,y)
                plt.title(group)
                plt.grid()
                plt.show()
    else:
        colour = 0
        nPlots = 0
        for group in sorted(orderedData):
            plot = 1
            for sorting in sort:
                tag = re.split(":",sorting)
                if len(re.findall(sorting,group)) == 0 and not (includeMissing and len(re.findall(tag[0],group)) == 0):
                    plot = 0
            nPlots += plot
        nPlots = nPlots - (1 if nPlots > 1 else 0)         
        colourInc = int(256/nPlots)
        lengths = {}
        for group in sorted(orderedData):
            plot = True
            for sorting in sort:
                tag = re.split(":",sorting)
                if len(re.findall(sorting,group)) == 0 and not (includeMissing and len(re.findall(tag[0],group)) == 0):
                    plot = False
            if plot:
                xMax = max([run.data[-1].timestep for run in orderedData[group]])
                x = list(range(int(xMax)))
                y = [np.mean([run.data[min(len(run.data)-1,dx)].value for run in orderedData[group]]) for dx in x]
                y = smoothData(y)
                nameTags = re.split("-",group)
                name = ""
                for key in nameTags:
                    add = True
                    for label in hideLabelKeys:
                        if label in key and label != '':
                            add = False
                    if add:
                        name += key
                        name += "-"
                name = name[:-1]
                lengths[name] = [run.data[-1].timestep for run in orderedData[group]]
                name = replaceTerms(name)

                plt.plot(x,y,label=name,color=cmap(colour))
                # print(f"{name}: {y[-1]}")

                if showStd:
                    mean = [np.mean([run.data[min(len(run.data)-1,dx)].value for run in orderedData[group]]) for dx in x]
                    std = [0.5*np.std([run.data[min(len(run.data)-1,dx)].value for run in orderedData[group]]) for dx in x]
                    mx = max([np.max([run.data[min(len(run.data)-1,dx)].value for run in orderedData[group]]) for dx in x])
                    mn = min([np.min([run.data[min(len(run.data)-1,dx)].value for run in orderedData[group]]) for dx in x])
                    quar1 = [max(mn,av-s) for av,s in zip(mean,std)]
                    quar2 = [min(mx,av+s) for av,s in zip(mean,std)]
                    quar1 = smoothData(quar1)
                    quar2 = smoothData(quar2)
                
                    plt.fill_between(x,quar1,quar2,color =cmap(colour),alpha = 0.2)
                colour += colourInc
            # plt.title(group)
        if title is not None:
            plt.title(title)
        plt.xlabel("Epochs")
        if ylabel is not None:
            plt.ylabel(ylabel)
        for name in lengths:
            print('{}:{}'.format(name,lengths[name]))
        plt.grid()
        plt.legend()
        plt.show()
    
    
                




    

