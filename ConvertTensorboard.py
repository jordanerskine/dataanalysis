from tensorboard.backend.event_processing import event_accumulator
import os

from websockets import Data
from dataUtil import Run, DataPoint

class MultiTensorboardConverter:
    def __init__(self, log_dir = ""):
        folders = os.listdir(log_dir)
        self.converters = []
        for folder in folders:
            self.converters.append(TensorboardConverter(log_dir+"/"+folder))
        
    def getRuns(self, tag):
        for con in self.converters:
            con.getRun(tag)
        

class TensorboardConverter:
    def __init__(self, log_dir = ""):
        self.log_dir = log_dir
        files = os.listdir(log_dir)
        fileName = [f for f in files if "event" in f][0]
        self.fileName = log_dir+"/"+fileName
        self.ea = event_accumulator.EventAccumulator(self.fileName)
        self.ea.Reload()
        self.tags = self.ea.Tags()


    def getRun(self, tag):
        variant = self.getVariant()
        self.run = Run(variant,self.log_dir.split("/")[-1])
        if tag in self.tags['scalars']:
            data = self.ea.Scalars(tag)
            for dat in data:
                self.run.data.append(DataPoint(dat.value,dat.step))


    def getVariant(self, file = "args.txt"):
        with open(self.log_dir+"/"+file,'r') as f:
            data = f.read()
        variant = {}
        for dat in data.split("\n"):
            if ": " in dat:
                key = dat.split(": ")[0]
                value = dat.split(": ")[1]
                variant[key] = value
        return variant

if __name__ == "__main__":
    T = TensorboardConverter("../transition/ModMaze2.Mar22/CheckpointMaze_2.proximity.Mar22_1")
    T.getRun("summary/success")