import tkinter as tk
import sys, csv, os, json, matplotlib.pyplot as plt, numpy as np, re, yaml, time, math
import matplotlib
from matplotlib.backends.backend_agg import FigureCanvas
from matplotlib.figure import Figure
from copy import deepcopy as copy
from functools import partial
from natsort import natsorted, ns

from ConvertTensorboard import MultiTensorboardConverter
from dataUtil import *

from PIL import Image, ImageTk
matplotlib.use('TKAgg')


class OverwritePlotWindow:
    def __init__(self, parent):
        self.parent = parent
        self.legendVars = None
        self.colourVars = None
        
        self.titleVar = tk.StringVar()
        self.xLabVar = tk.StringVar()
        self.yLabVar = tk.StringVar()
        self.seedVar = tk.StringVar()
        
        self.xMin = tk.StringVar()
        self.xMax = tk.StringVar()
        self.xScale = tk.StringVar()
        self.yMin= tk.StringVar()
        self.yMax = tk.StringVar()
        self.yScale = tk.StringVar()

        self.colWin = None


        
    def initWindow(self):
        self.nEntries = self.parent.nPlots

        self.top = tk.Toplevel(self.parent.window)
        self.top.title("Overwrite plot parameters")

        self.FigOptFrame = tk.Frame(self.top,borderwidth=2,relief='ridge')
        self.FigOptFrame.grid(column=0,row=0)

        self.titleLabel = tk.Label(self.FigOptFrame,text = "Title")
        self.titleLabel.grid(column=0,row=0)


        self.titleEntry = tk.Entry(self.FigOptFrame,textvariable = self.titleVar,width = 20)
        self.titleEntry.grid(column=1,row=0)

        self.xLabLabel = tk.Label(self.FigOptFrame,text = "X Label")
        self.xLabLabel.grid(column=0,row=1)

        self.xLabEntry = tk.Entry(self.FigOptFrame,textvariable = self.xLabVar,width = 20)
        self.xLabEntry.grid(column=1,row=1)

        self.yLabLabel = tk.Label(self.FigOptFrame,text = "Y Label")
        self.yLabLabel.grid(column=0,row=2)

        self.yLabEntry = tk.Entry(self.FigOptFrame,textvariable = self.yLabVar,width = 20)
        self.yLabEntry.grid(column=1,row=2)

        self.seedFrame = tk.LabelFrame(self.top, text = "Which runs to use")
        self.seedFrame.grid(row = 1)

        self.seedEntry = tk.Entry(self.seedFrame, text = self.seedVar, width = 20)
        self.seedEntry.pack()

        self.axisFrame = tk.Frame(self.top)
        self.axisFrame.grid(row = 2)
        
        self.xFrame = tk.LabelFrame(self.axisFrame,text = "X Range")
        self.xFrame.pack()

        tk.Label(self.xFrame, text = "Min").grid(row=0, column=0)
        self.xMinEntry = tk.Entry(self.xFrame, textvariable=self.xMin,width = 6)
        self.xMinEntry.grid(row = 1, column = 0)
        tk.Label(self.xFrame, text = "Max").grid(row=0, column=1)
        self.xMaxEntry = tk.Entry(self.xFrame, textvariable=self.xMax,width = 6)
        self.xMaxEntry.grid(row = 1, column = 1)
        tk.Label(self.xFrame, text = "Scale").grid(row=0, column=2)
        self.xScaleEntry = tk.Entry(self.xFrame, textvariable=self.xScale,width = 6, relief='ridge')
        self.xScaleEntry.grid(row = 1, column = 2)

        self.yFrame = tk.LabelFrame(self.axisFrame,text = "Y Range")
        self.yFrame.pack()

        tk.Label(self.yFrame, text = "Min").grid(row=0, column=0)
        self.yMinEntry = tk.Entry(self.yFrame, textvariable=self.yMin,width = 6)
        self.yMinEntry.grid(row = 1, column = 0)
        tk.Label(self.yFrame, text = "Max").grid(row=0, column=1)
        self.yMaxEntry = tk.Entry(self.yFrame, textvariable=self.yMax,width = 6)
        self.yMaxEntry.grid(row = 1, column = 1)
        tk.Label(self.yFrame, text = "Scale").grid(row=0, column=2)
        self.yScaleEntry = tk.Entry(self.yFrame, textvariable=self.yScale,width = 6, relief='ridge')
        self.yScaleEntry.grid(row = 1, column = 2)


        self.legendEntryFrame = tk.LabelFrame(self.top,text = "Legend Update")
        self.legendEntryFrame.grid(column = 0, row = 3)

        self.legendEntry = []
        self.legendColourEntry = []
        self.baselineEntry = []
        self.showEntry = []
        if self.legendVars is None:
            self.legendVars = []
            self.colourVars = []
            self.baselineVars = []
            self.showVars = []
            for i in range(self.nEntries):
                self.legendVars.append(tk.StringVar(value = ""))
                colour = [tk.StringVar(value = "") for _ in range(3)]
                self.colourVars.append(colour)
                self.baselineVars.append(tk.BooleanVar(value = False))
                self.showVars.append(tk.BooleanVar(value = True))
        for i in range(self.nEntries):
            self.legendEntry.append(tk.Entry(self.legendEntryFrame,textvariable = self.legendVars[i]))
            self.legendEntry[i].grid(row = i,column = 0)
            
            win = partial(self.openColourWindow,i)
            self.legendColourEntry.append(tk.Button(self.legendEntryFrame,text = "Colour", command=win))
            self.legendColourEntry[i].grid(row = i,column = 1)

            self.baselineEntry.append(tk.Checkbutton(self.legendEntryFrame, text = "Baseline", variable = self.baselineVars[i]))
            self.baselineEntry[i].grid(row = i,column = 2)

            self.showEntry.append(tk.Checkbutton(self.legendEntryFrame, text = "Show", variable = self.showVars[i]))
            self.showEntry[i].grid(row=i,column=3)
            
        
        self.closeButton = tk.Button(self.top,text = "Close",command = self.close)
        self.closeButton.grid(row = 4,column = 0)

    def openColourWindow(self,entry):
        if self.colWin is not None:
            self.colWin.destroy()
        self.colWin = tk.Toplevel(self.top)
        self.colWin.title("Colour")
        self.colourEntry = []
        colours = ["R","G","B"]
        for j in range(3):
            tk.Label(self.colWin, text = colours[j]).grid(row=0,column=j)
            self.colourEntry.append(tk.Entry(self.colWin,textvariable=self.colourVars[entry][j],width = 3))
            self.colourEntry[j].grid(row = 1,column = j)
               


    def getColours(self):
        colours = []
        for i in range(self.nEntries):
            colour = []
            for j in range(3):
                channel = self.colourVars[i][j].get()
                if channel =="" or float(channel) < 0 or float(channel)/255 > 1:
                    colour = None
                    break
                colour.append(float(channel)/255)
            colours.append(colour)
        return colours

    def close(self):
        self.top.withdraw()

class SelectTagsWindow:
    def __init__(self, parent, tags = [], variants = []):
        self.parent = parent
        self.tagStates = {}
        self.variants = variants
        for tag in tags:
            self.tagStates[tag] = {"Ignore":tk.BooleanVar(),
                                   "Hide":tk.BooleanVar(),
                                   "Filter":tk.StringVar(value="All")}


    def initWindow(self):
        self.top = tk.Toplevel(self.parent.window)
        self.top.title("Tag Filter")

        self.tagNames = []
        self.tagIgnores = []
        self.tagHides = []
        self.tagFilters = []

        self.optFrame = tk.Frame(self.top,relief='ridge',borderwidth=3)
        self.optFrame.grid(row = 1)
        self.canvas = tk.Canvas(self.optFrame,width=600,height = 700)
        self.scroll_y = tk.Scrollbar(self.optFrame, orient = "vertical", command=self.canvas.yview)

        frame = tk.Frame(self.canvas)

        titleFrame = tk.Frame(frame, relief = 'solid',borderwidth=3,height=1)
        titleFrame.grid(row = 1, columnspan = 4)

        self.tagNameLabel = tk.Label(frame, text = "Tag",font = 'Helvetica 12 bold')
        self.tagNameLabel.grid(row = 0,column = 0)

        self.tagIgnoreLabel = tk.Label(frame, text = "Ignore",font = 'Helvetica 12 bold')
        self.tagIgnoreLabel.grid(row = 0,column = 1)

        self.tagHideLabel = tk.Label(frame, text = "Hide",font = 'Helvetica 12 bold')
        self.tagHideLabel.grid(row = 0,column = 2)

        self.tagFilterLabel = tk.Label(frame, text = "Filter",font = 'Helvetica 12 bold')
        self.tagFilterLabel.grid(row=0, column = 3)

        for i, tag in enumerate(self.tagStates):
            self.tagNames.append(tk.Label(frame,text = tag))
            self.tagNames[-1].grid(row = i+2, column = 0)

            self.tagIgnores.append(tk.Checkbutton(frame, variable=self.tagStates[tag]["Ignore"]))
            self.tagIgnores[-1].grid(row = i+2, column = 1)

            self.tagHides.append(tk.Checkbutton(frame, variable=self.tagStates[tag]["Hide"]))
            self.tagHides[-1].grid(row = i+2, column = 2)

            t = tag.replace(":","']['")
            vals = []
            for var in self.variants:
                try:
                    vals.append(eval(f"str(var['{t}'])"))
                except:
                    pass
            vals = tuple(['All'] + list(np.unique(vals)))
            if len(vals) == 2:
                self.tagStates[tag]["Hide"].set(True)
            self.tagFilters.append(tk.OptionMenu(frame, self.tagStates[tag]["Filter"], *vals))
            self.tagFilters[-1].grid(row = i+2, column = 3)

        self.canvas.create_window(0,0,anchor='nw',window = frame)
        self.canvas.update_idletasks()
        self.canvas.configure(scrollregion=self.canvas.bbox('all'),
                                yscrollcommand=self.scroll_y.set)

        self.canvas.pack(fill='both', expand=True, side = 'left')
        self.scroll_y.pack(fill = 'y',side = 'right')
            
        
        self.closeButton = tk.Button(self.top, text = "Close", command = self.close)
        self.closeButton.grid(row = 1000, column = 0)

    def getHiddenTags(self):
        tags = []
        for tag in self.tagStates:
            if self.tagStates[tag]["Hide"].get():
                tags.append(tag.split(":")[-1])
        return tags

    def getIgnoredTags(self):
        tags = []
        for tag in self.tagStates:
            if self.tagStates[tag]["Ignore"].get():
                tags.append(tag.split(":")[-1])
        return tags

    def getFilteredTags(self):
        tags = []
        for tag in self.tagStates:
            if self.tagStates[tag]["Filter"].get() != "All":
                label = [t[0] for t in tag.split(":")[:-1]]
                label.append(tag.split(":")[-1])
                tags.append("/".join(label) + ":" + self.tagStates[tag]["Filter"].get())
        return tags


    
    def close(self):
        self.parent._updateVars()
        self.top.withdraw()


class InteractiveWindow:
    def __init__(self, path = None, fileName = 'env_info'):
               

        self.window = tk.Tk()
        self.window.title("Interactive Analysis")
        

        self.path = path
        self.smoothingFactor = 100000
        self.sort = ""
        self.includeMissing = 0
        self.ignoreKeys = ""
        self.showAll = True
        self.hideLabelKeys = []
        self.showStd = True
        self.fileName = fileName
        self.latestVar = None
        self.zeroStart = 0
        self.additionalData = None

        self.nPlots = 5
        
        self.folder = {}
        for p in path.split(","):
            self.folder[p] = os.listdir(p)
        
        
        firstFileFolder = "{}/{}".format(path.split(",")[0],os.listdir(path.split(",")[0])[0])
        self.fileOptions = [f.split(".")[0] for f in os.listdir(firstFileFolder) if "." in f and f.split(".")[1] == "csv"]
        if self.fileName not in self.fileOptions:
            print(f"{self.fileName}.csv was not present. Defaulting to {self.fileOptions[0]}.csv")
            self.fileName = self.fileOptions[0]

        with open("{}/{}/{}.csv".format(path.split(",")[0],self.folder[path.split(",")[0]][0],self.fileName),'r',newline='') as f:
            self.vars = list(csv.reader(f, delimiter=','))[0]
        

        self.figOptions = OverwritePlotWindow(self)
        self.selectTags = None

        self.initOptions()
        self.initAdditionalOptions()
        
        array = np.ones((480,640,4))*50
        array = array.astype(np.uint8)
        self.array = array
        
        self.currentImage = ImageTk.PhotoImage(image = Image.fromarray(array))
        
        # self.canvas = tk.Canvas(self.window,width=640,height=480,bg="white")
        try:
            self.canvas = tk.Label(self.window,image = self.currentImage)
        except:
            self.canvas = tk.Label(self.window)
        # self.currentPlot = self.image.create_image(0,0,image = self.currentImage)
        self.canvas.grid(column=0,row=2,columnspan=2,sticky='W')

        self.initSave()

        self.window.protocol("WM_DELETE_WINDOW",self.close_window)

        
        self.window.mainloop()

    def close_window(self):
        sys.exit(101)

    def initOptions(self):
        self.optionsFrame = tk.Frame(self.window,borderwidth=2,relief='ridge')
        self.optionsFrame.grid(column=0, row=0, rowspan=2)

        
        self.extensionVar = tk.Variable(value=self.fileName)
        self.extensionLabel = tk.Label(self.optionsFrame,text="File Name")
        self.extensionLabel.grid(column=0,row=0)

        self.extensionEntry = tk.OptionMenu(self.optionsFrame,self.extensionVar,*self.fileOptions)
        self.extensionEntry.grid(column=1,row=0)

        self.extensionLoadButton = tk.Button(self.optionsFrame, text = 'Load Files', command=self._updateVarList)
        self.extensionLoadButton.grid(column=0, row=1, columnspan=2)

        

        self.varLabel = tk.Label(self.optionsFrame,text="Variable")
        self.varLabel.grid(column=0,row=2)

        
        self.varSpin = tk.Spinbox(self.optionsFrame,from_ = 0, to = len(self.vars)-1, command = self._showVarName, width=3)
        self.varSpin.grid(column=1,row=2)

        self.varName = tk.Label(self.optionsFrame,width=50)
        self.varName.grid(column=0,row=3, columnspan=2)

        self._updateVarList()

        self.plotButton = tk.Button(self.optionsFrame,text="Plot Variable",command=self.getPlot)
        self.plotButton.grid(column=0,row=5,columnspan=5)

        self.showAllVar = tk.Variable(value=0)
        self.showAllButton = tk.Checkbutton(self.optionsFrame,text = "Show All",variable=self.showAllVar,command=self._updateVars)
        self.showAllButton.grid(column=2,row = 0, sticky="W")

        self.showStdVar = tk.Variable(value='1')
        self.showStdButton = tk.Checkbutton(self.optionsFrame,text="Show STD",variable=self.showStdVar)
        self.showStdButton.grid(column=2,row=1, sticky="W")
        
        self.showLegendVar = tk.Variable(value='1')
        self.showLegendButton = tk.Checkbutton(self.optionsFrame,text="Show Legend",variable=self.showLegendVar)
        self.showLegendButton.grid(column=2,row=2, sticky="W")

        self.outLegendVar = tk.Variable(value='1')
        self.outLegendButton = tk.Checkbutton(self.optionsFrame,text="Legend Outside",variable = self.outLegendVar)
        self.outLegendButton.grid(column=2,row=3, sticky="W")

        self.includeMissingVar = tk.Variable(value='0')
        self.includeMissingButton = tk.Checkbutton(self.optionsFrame,text="Include Missing",variable = self.includeMissingVar)
        self.includeMissingButton.grid(column=3,row=0, sticky="W")

        self.autoResolutionVar = tk.Variable(value='1')
        self.autoResolutionButton = tk.Checkbutton(self.optionsFrame,text="Auto-Resolution", variable = self.autoResolutionVar)
        self.autoResolutionButton.grid(column=3,row=1, sticky="W")

        self.zeroStartVar = tk.Variable(value='1')
        self.zeroStartButton = tk.Checkbutton(self.optionsFrame,text="Start @ 0", variable = self.zeroStartVar)
        self.zeroStartButton.grid(column=3,row=2, sticky="W")

        self.hideUncommonTagsVar = tk.Variable(value = '0')
        # self.hideUncommonTagsButton = tk.Checkbutton(self.optionsFrame,text="Hide Uncommon Tags", variable = self.hideUncommonTagsVar)
        # self.hideUncommonTagsButton.grid(column=3,row=3, sticky="W")
        self.simplifiedFunctionsVar = tk.Variable(value = '0')
        self.simplifiedFunctionsButton = tk.Checkbutton(self.optionsFrame, text = "Simplify Functions", variable = self.simplifiedFunctionsVar)
        self.simplifiedFunctionsButton.grid(column=3,row=3, sticky="W")

        
        self.sortFrame = tk.Frame(self.optionsFrame,borderwidth=2,relief="ridge")
        # self.sortFrame.grid(column=4,row=0, sticky="W")
        self.sortLabel = tk.Label(self.sortFrame,text = "Sort")
        # self.sortLabel.grid(column=0,row=0)

        self.sortVar = tk.StringVar()
        self.sortVarBar = tk.Entry(self.sortFrame,textvariable=self.sortVar,width=30)
        # self.sortVarBar.grid(column=1,row=0)

        self.smoothFrame = tk.Frame(self.optionsFrame,borderwidth=2,relief="ridge")
        self.smoothFrame.grid(column=4,row=1, sticky="W")
        self.smoothLabel = tk.Label(self.smoothFrame,text="Smoothing Factor")
        self.smoothLabel.grid(column=0,row=0)

        self.smoothVar = tk.Variable(value = self.smoothingFactor)
        self.smoothVarBar = tk.Entry(self.smoothFrame,textvariable = self.smoothVar,width=10)
        self.smoothVarBar.grid(column=1,row=0)

        self.ignoreFrame = tk.Frame(self.optionsFrame,borderwidth=2,relief="ridge")
        # self.ignoreFrame.grid(column=4,row=2, sticky="W")
        self.ignoreLabel = tk.Label(self.ignoreFrame,text = "Ignore tags")
        # self.ignoreLabel.grid(column=0,row=0)

        self.ignoreVar = tk.StringVar()
        self.ignoreVarBar = tk.Entry(self.ignoreFrame,textvariable=self.ignoreVar,width=30)
        # self.ignoreVarBar.grid(column=1,row=0)

        self.hideFrame = tk.Frame(self.optionsFrame,borderwidth=2,relief="ridge")
        # self.hideFrame.grid(column=4,row=3, sticky="W")
        self.hideLabelLabel = tk.Label(self.hideFrame,text = "Hide Labels")
        # self.hideLabelLabel.grid(column=0,row=0)


        self.hideLabelVar = tk.StringVar()
        self.hideLabelBar = tk.Entry(self.hideFrame,textvariable=self.hideLabelVar,width=30)
        # self.hideLabelBar.grid(column=1,row=0)
        self.selectVarsButton = tk.Button(self.optionsFrame, text = "Select Variables", command = self.initSelectTags)
        self.selectVarsButton.grid(column = 4, row = 0)
        


        self.optionsPaneDim = dict(h=146,w=868)

    def initSave(self):
        self.saveFrame = tk.Frame(self.window,borderwidth=2,relief="ridge")
        self.saveFrame.grid(column=0,row=3,columnspan=2)

        self.saveNameLabel = tk.Label(self.saveFrame,text = "Name")
        self.saveNameLabel.grid(column=0,row=0)

        self.saveName = tk.StringVar()
        self.saveNameEntry = tk.Entry(self.saveFrame,textvariable = self.saveName,width=30)
        self.saveNameEntry.grid(column=0,row=1)

        self.saveButton = tk.Button(self.saveFrame,text = "Save Figure",command = self.saveFigure)
        self.saveButton.grid(column=0,row=2)

        self.savePaneDim = dict(h=80,w=100)


    def initAdditionalOptions(self):

        self.openOptionButton = tk.Button(self.window,text = "Modify Figure", command = self.changeFigure)
        self.openOptionButton.grid(row = 0, column = 1)

        self.AddOptFrame = tk.LabelFrame(self.window, text = "Import Tensorboard Files",borderwidth=2,relief='ridge')
        self.AddOptFrame.grid(column = 1, row = 1)

        self.AddExternalButton = tk.Button(self.AddOptFrame, text = "Add Additional Data", command = self.updateAdditionDataLocation)
        self.AddExternalButton.grid(row = 0,column = 1)

        self.LoadAdditionalButton = tk.Button(self.AddOptFrame, text = "Load", command = self.getAdditionalData)
        self.LoadAdditionalButton.grid(row = 0, column = 0)

        self.AdditonalFolderVar = tk.StringVar(value = "./")
        self.AdditionalDataFolderEntry = tk.Entry(self.AddOptFrame, textvariable=self.AdditonalFolderVar)
        self.AdditionalDataFolderEntry.grid(row = 1,columnspan = 2)

        self.tagEntryVar = tk.StringVar(value = "summary/success")
        self.additionalTagLabel = tk.Label(self.AddOptFrame, text = "Tag")
        self.additionalTagLabel.grid(row = 2, column = 0)
        self.tagEntry = tk.Entry(self.AddOptFrame, textvariable=self.tagEntryVar)
        self.tagEntry.grid(row = 2, column = 1)

    def changeFigure(self):
        self.figOptions.initWindow()

    def initSelectTags(self):
        if self.selectTags is not None:
            self.selectTags.initWindow()

    def _showVarName(self):
        self.varName['text'] = self.vars[int(self.varSpin.get())]

    def saveFigure(self):
        fig = self.canvas.image
        name = self.saveName.get()
        d = "/media/jordan/Documents/Repos/DataAnalysis/Figures"
        try:
            os.makedirs(d)
        except:
            pass
        fig._PhotoImage__photo.write(f"{d}/{name}.png")


    def _updateVarList(self):
        self.fileName = self.extensionVar.get()
        self.vars = []
        path = list(self.folder.keys())[0]
        for i, run in enumerate(self.folder[path]):
            try:
                with open("{}/{}/{}.csv".format(path,run,self.fileName),'r',newline='') as f:
                    # print(self.folder[0])
                    text = f.read()
                    vars = text.split("\r\n")[0].split(",")
                    for var in vars:
                        if var not in self.vars:
                            self.vars.append(var)
                    self.vars.sort()
            except:
                pass
        self.varSpin.config(to=len(self.vars)-1)
        
        self._showVarName()

    def replaceTerms(self,string):
        replStrs = [['name',''],
                    [':coopSac','CSAC (ours)'],
                    [':sac','SAC'],
                    [':uncoopSac','naive SAC']]
        for inp,out in replStrs:
            string = re.sub(inp,out,string)

        return string
        

    def _updateVars(self):
        self.showAll = True if self.showAllVar.get() == '1' else False
        self.showStdButton['state'] = 'disabled' if self.showAll else 'normal'
        self.showStd = True if self.showStdVar.get() == '1' else False
        self.autoResolution = True if self.autoResolutionVar.get() == '1' else False
        self.sort = [] if self.selectTags is None else self.selectTags.getFilteredTags()
        self.smoothingFactor = float(self.smoothVar.get())
        self.showLegend = True if self.showLegendVar.get() == '1' else False
        self.ignoreKeys = [] if self.selectTags is None else self.selectTags.getIgnoredTags()
        self.includeMissing = True if self.includeMissingVar.get() == '1' else False
        self.zeroStart = True if self.zeroStartVar.get() == '1' else False
        self.hideLabelKeys = [] if self.selectTags is None else self.selectTags.getHiddenTags()
        self.outLegend = True if self.outLegendVar.get() == '1' else False
        self.outLegendButton['state'] = 'disabled' if not self.showLegend else 'normal'
        self.hideUncommonTags = True if self.hideUncommonTagsVar.get() == '1' else False
        self.simplifiedFunctions = True if self.simplifiedFunctionsVar.get() == '1' else False

    def getKeyVariables(self,variants):
        
        keys = self.compareDicts([self.unpeelDict(v) for v in variants])
        if self.selectTags is None:
            self.selectTags = SelectTagsWindow(self,keys,[run.variant for run in self.data])
        return keys
         
    def unpeelDict(self,dct):
        newDict = {}
        for elem in dct:
            if isinstance(dct[elem],dict):
                smallDict = self.unpeelDict(dct[elem])
                for smallElem in smallDict:
                    newDict[f'{elem}:{smallElem}'] = smallDict[smallElem]
            else:
                newDict[elem] = dct[elem]
        return newDict

    def compareDicts(self,dicts):
        keys = []
        for d in dicts:
            for key in d:
                for dd in dicts:
                    if not (key in dd and dd[key] == d[key]):
                        if key not in keys:
                            keys.append(key)
        
        if self.hideUncommonTags:
            newKeys = []
            for key in keys:
                if np.all([key in d for d in dicts]):
                    newKeys.append(key)
            keys = newKeys
        return keys


    def smoothRun(self,vals,times):
        
        newData = [vals[0]]
        for i in range(1,len(vals)):

            smooth = (0.05**(1/(self.smoothingFactor)))**(times[i]-times[i-1])
            newData.append(newData[-1]*smooth+(1-smooth)*vals[i])
        return newData

    def interpolateData(self,data,interval):
        newData = Run(data.variant, data.name)
        lastPoint = 0
        finalTimestep = int(data.data[-1].timestep)
        for t in range(0,finalTimestep,interval):
            while lastPoint < len(data.data)-1 and t > data.data[lastPoint+1].timestep:
                lastPoint += 1
            if t < data.data[lastPoint].timestep:
                val = data.data[lastPoint].value
            else:
                val = self.interpolate(data.data[lastPoint].value,
                                       data.data[lastPoint].timestep,
                                       data.data[lastPoint+1].value,
                                       data.data[lastPoint+1].timestep,
                                       t)
                if t > data.data[lastPoint+1].timestep:
                    t
            
            newData.data.append(DataPoint(value=val,timestep=t))
        # print(f'Old Max:{max([d.value for d in data.data])}')
        # print(f'Old Min:{min([d.value for d in data.data])}')
        # print(f'New Max:{max([d.value for d in newData.data])}')
        # print(f'New Min:{min([d.value for d in newData.data])}')
        return newData

    def interpolate(self,x1,y1,x2,y2,yt):
        return x1+(yt-y1)*(x2-x1)/(y2-y1)
    
    def getTimeName(self,tags):
        for tag in ['step','episode','Epoch']:
            if tag in tags:
                return tag
        print("No appropriate time tag")

    
    def updateAdditionDataLocation(self):
        folderLocation = tk.filedialog.askopenfilename(initialdir = self.AdditonalFolderVar.get(),title = "Get Additional Data Location")
        folderLocation = "/".join(folderLocation.split("/")[:-2])
        self.AdditonalFolderVar.set(folderLocation)

    def getAdditionalData(self):
        importObject = MultiTensorboardConverter(self.AdditonalFolderVar.get())
        importObject.getRuns(self.tagEntryVar.get())
        data = [con.run for con in importObject.converters if len(con.run.data)>0]
        keys = self.getKeyVariables([con.run.variant for con in importObject.converters])
        orderedData = {}
        for run in data:

            name = ""
            for key in keys:
                ks = re.split(":",key)
                if not(self.ignoreKeys is not None and ks[-1] in self.ignoreKeys):
                    try:
                        if len(ks)==2:
                            name += "{}:{}~".format(ks[1],run.variant[ks[0]][ks[1]])
                        elif len(ks)==3:
                            name += "{}:{}~".format(ks[2],run.variant[ks[0]][ks[1]][ks[2]])
                        else:
                            name += "{}:{}~".format(key,run.variant[key])
                    except:
                        pass
            name = name[:-1]
            if name not in orderedData:
                orderedData[name] = [run]
            else:
                orderedData[name].append(run)

        self.additionalData = orderedData


    def _importData(self,var):
        print("Starting data import\n--------")
        self.data = []
        self.stepSize = 0
        for path in self.folder:
            folder = self.folder[path]
            for runNum, run in enumerate(folder):
                start = time.time()
                t = 0
                try:
                    with open("{}/{}/{}.csv".format(path,run,self.fileName),'r',newline='') as f:
                        text = f.read()
                        reader = [line.split(",") for line in text.split("\r\n")]
                        # reader = list(csv.reader(f, delimiter=','))
                        if self.stepSize == 0:
                            if self.autoResolution:
                                self.stepSize = 10**(max(0,int(math.log10(len(reader)))-3))
                            else:
                                self.stepSize = 1
                        timeName = self.getTimeName(reader[0])
                        epochTag = reader[0].index(timeName)
                        if var in reader[0]:
                            tagPresent = True
                            varTag = reader[0].index(var)
                            try:
                                with open("{}/{}/variant.json".format(path,run),'r') as j:
                                    variant = json.load(j)
                                    if len(reader) > self.stepSize:
                                        self.data.append(Run(variant, run))
                            except:
                                try:
                                    with open(f"{path}/{run}/.hydra/config.yaml","r") as j:
                                        config = yaml.load(j,Loader=yaml.FullLoader)
                                        if len(reader) > self.stepSize:
                                            self.data.append(Run(config, run))
                                except:
                                    with open(f"{path}/{run}/config.json") as j:
                                        variant = json.load(j)
                                        if len(reader) > self.stepSize:
                                            self.data.append(Run(variant, run))
                            for rowN in range(1,len(reader[1:]),self.stepSize):
                                row = reader[rowN]
                                val = float(row[varTag]) if not (self.zeroStart and rowN==1) else 0
                                t = float(row[epochTag])
                                self.data[-1].data.append(DataPoint(val,t))
                            if len(self.data[-1].data) ==0:
                                del self.data[-1]
                            
                            self.smoothVar.set(0.1 * round(t, -int(math.floor(math.log10(abs(t))))))
                    print(f"{run} loaded. Took {time.time()-start} seconds")
                                
                except:
                    print("{} failed to read".format(run))
                    print("Last time: {}".format(t))
        self.timeName = timeName
        return self.data

    def testInterpolation(self, run, mx = None, smooth = False):
        x1 = run.listTimes()
        y1 = run.listValues()
        irun = self.interpolateData(run,100)
        x2 = irun.listTimes()
        y2 = irun.listValues()
        if smooth:
            y1 = self.smoothRun(y1,x1)
            y2 = self.smoothRun(y2,x2)

        plt.figure()
        if mx is None:
            plt.plot(x1,y1,label = "Without interpolation")
            plt.plot(x2,y2,label = "With interpolation")
        else:
            t = np.argmin(np.abs(np.array(x1)-mx))-1
            plt.plot(x1[:t],y1[:t],label = "Without interpolation")
            t = np.argmin(np.abs(np.array(x2)-mx))-1
            plt.plot(x2[:t],y2[:t],label = "With interpolation")
        plt.legend()
        plt.show()

    
    def getSimplifiedFunction(self, x,y,showBestSlope=True, showFinalPerformance = True):
        x = x[:-1]
        y = y[:-1]
        points = []
        if showBestSlope:
            xRange = np.max(x)-np.min(x)
            yRange = np.max(y)-np.min(y)

            def cost(x1,x2,y1,y2):
                yDiff = (y2-y1)/yRange
                xDiff = (x2-x1)/xRange
                slope = yDiff/xDiff
                length = np.linalg.norm([xDiff,yDiff])
                return slope**(1/3)+0.4*length
            
            step = int(len(y)/20)
            bestCost = 0
            bestPoints = []
            for start in np.arange(0,len(y)-1,step):
                for end in np.arange(start+step,len(y)+step,step):
                    end = min(end,len(y)-1)
                    c = cost(x[start],x[end],y[start],y[end])
                    if c > bestCost:
                        bestCost = c
                        bestPoints = [start,end]
            slopePoints = [(x[bestPoints[0]],y[bestPoints[0]]),(x[bestPoints[1]],y[bestPoints[1]])]
            points.extend(slopePoints)
        
        if showFinalPerformance:
            finalPerformanceDeviationAmount = 0.00

            finalPerformance = np.mean(y[len(y)-20:])
            ind = list(y>=finalPerformance*(1-finalPerformanceDeviationAmount)).index(True)
            finalPoint = [(x[ind],y[ind])]
            points.extend(finalPoint)
        return np.array([p[0] for p in points]),np.array([p[1] for p in points])
            


        

    def getPlot(self):
        self._updateVars()
        
        var = self.varName['text']
        
        if var != self.latestVar:
            data = self._importData(var)
            self.latestData = data
            self.latestVar = var
        else:
            data = self.latestData

        print(f"h:{self.window.winfo_height()},w:{self.window.winfo_width()}")
        scale = 0.01
        size = (scale*self.window.winfo_width(),
                scale*(self.window.winfo_height()-self.optionsPaneDim["h"]-self.savePaneDim["h"]))
        
        fig = plt.figure(figsize=size)

        
        orderedData = {}
        variants = []
        keys = self.getKeyVariables([run.variant for run in data])
        print(f'Key variables: \n{keys}')
        for run in data:

            name = ""
            for key in keys:
                ks = re.split(":",key)
                if not(self.ignoreKeys is not None and ks[-1] in self.ignoreKeys):
                    try:
                        label = [k[0] for k in ks[:-1]]
                        label.append(ks[-1])
                        delim = "/"
                        label = delim.join(label)
                        if len(ks)==2:
                            name += "{}:{}~".format(label,run.variant[ks[0]][ks[1]])
                        elif len(ks)==3:
                            name += "{}:{}~".format(label,run.variant[ks[0]][ks[1]][ks[2]])
                        else:
                            name += "{}:{}~".format(label,run.variant[key])
                    except:
                        pass
            name = name[:-1]
            if name not in orderedData:
                orderedData[name] = [run]
            else:
                orderedData[name].append(run)

        # ----- Add additional data to orderedData
        if self.additionalData is not None:
            orderedData.update(self.additionalData)

        cmap = plt.get_cmap('jet')
        if self.showAll:
            
            for group in natsorted(orderedData, alg=ns.REAL):
                plot = True
                for sorting in self.sort:
                    if not max([sorting == g for g in group.split("~")]) and not (self.includeMissing and sorting.split(":")[0] not in group):
                        plot = False
                if plot:
                    
                    if self.figOptions.seedVar.get() == "":
                        runs = [run for run in orderedData[group]]
                    else:
                        inds = np.array(self.figOptions.seedVar.get().split(",")).astype(int)
                        runs = [run for i, run in enumerate(orderedData[group]) if i in inds]
                    for run in runs:
                        # self.testInterpolation(run)
                        interval = int(10**(max(0,int(math.log10(run.data[-1].timestep))-3)))
                        irun = self.interpolateData(run,interval)
                        x = irun.listTimes()
                        y = irun.listValues()
                        y = self.smoothRun(y,x)
                        plt.plot(x,y,label=irun.name)
                    # plt.show()
        else:
            colour = 0
            nPlots = 0
            for group in natsorted(orderedData, alg=ns.REAL):
                plot = 1
                for sorting in self.sort:
                    if not max([sorting == g for g in group.split("~")]) and not (self.includeMissing and sorting.split(":")[0] not in group):
                        plot = 0
                nPlots += plot
            if self.figOptions.legendVars is not None:
                nPlots = sum([v.get() for v in self.figOptions.showVars])
            nPlots = nPlots - (1 if nPlots > 1 else 0)
            colourInc = int(256/nPlots)
            plotIt = 0
            lengths = {}
            for group in natsorted(orderedData, alg=ns.REAL):
                plot = True
                for sorting in self.sort:
                    if not max([sorting == g for g in group.split("~")]) and not (self.includeMissing and sorting.split(":")[0] not in group):
                        plot = False
                if (self.figOptions.legendVars is not None and plotIt < self.figOptions.nEntries and
                            not self.figOptions.showVars[plotIt].get() and plot):
                    plot = False
                    plotIt += 1
                
                if plot:
                    
                    xMax = int(max([run.data[-1].timestep for run in orderedData[group]]))
                    interval = int(10**(max(0,int(math.log10(xMax))-3)))
                    if self.figOptions.seedVar.get() == "":
                        runs = [self.interpolateData(run,interval) for run in orderedData[group]]
                    else:
                        inds = np.array(self.figOptions.seedVar.get().split(",")).astype(int)
                        runs = [self.interpolateData(run,interval) for i, run in enumerate(orderedData[group]) if i in inds]
                    # runs = orderedData[group]
                    x = range(0,xMax,interval)
                    # x = list(range(int(xMax)))
                    runYs = []
                    for run in runs:
                        timesteps = np.array([d.timestep for d in run.data])
                        runYs.append([run.data[np.argmin(np.abs(dx - timesteps))].value if dx < timesteps[-1] else np.inf for dx in x])
                    # runYs = [run.listValues() for run in runs]
                    # x = [run.listTimes() for run in runs]
                    runYs = np.ma.masked_values(runYs,np.inf)
                    mean = np.mean(runYs,axis=0)
                    y = self.smoothRun(mean,x)

                    if (self.figOptions.legendVars is not None and plotIt < self.figOptions.nEntries and
                                    self.figOptions.legendVars[plotIt].get() != ""):
                        name = self.figOptions.legendVars[plotIt].get()
                    if (self.figOptions.colourVars is not None and plotIt < self.figOptions.nEntries and 
                                    self.figOptions.getColours()[plotIt] is not None):
                        col = tuple(self.figOptions.getColours()[plotIt])
                    else:
                        col = cmap(colour)

                    if self.figOptions.xScale.get() != "":
                        nx = np.array(x)*float(self.figOptions.xScale.get())
                    else:
                        nx = x
                    nameTags = re.split("~",group)
                    name = ""
                    for key in nameTags:
                        add = True
                        for label in self.hideLabelKeys:
                            if label in key and label != '':
                                add = False
                        for sorting in self.sort:
                            tag = re.split(":",sorting)[0]
                            if tag in key and tag != '':
                                add = False 
                        if add:
                            name += key
                            name += "~"
                    name = name[:-1]
                    lengths[name] = [run.data[-1].timestep for run in orderedData[group]]
                    if (self.figOptions.legendVars is not None and plotIt < self.figOptions.nEntries and self.figOptions.legendVars[plotIt].get() != ""):
                        name = self.figOptions.legendVars[plotIt].get()


                    if (self.figOptions.legendVars is not None and plotIt < self.figOptions.nEntries and
                            self.figOptions.baselineVars[plotIt].get()):
                        lineStyle = 'dashed'
                        plt.axhline(y = [max(y)], label=name,color=col,linestyle=lineStyle)
                    else:
                        
                        # plt.plot(nx,y,label=name,color=col)
                        if self.simplifiedFunctions:
                            plt.xlim([np.min(nx)-(np.max(nx)-np.min(nx))*0.05,np.max(nx)+(np.max(nx)-np.min(nx))*0.05])
                            plt.ylim([np.min(y[:-1])-(np.max(y[:-1])-np.min(y[:-1]))*0.05,np.max(y[:-1])+(np.max(y[:-1])-np.min(y[:-1]))*0.05])
                            nx,y = self.getSimplifiedFunction(nx,y)

                        plt.plot(nx,y,label=name,color=col)

                        if self.showStd and not self.simplifiedFunctions:
                            std = [0.5*np.std([run.data[min(len(run.data)-1,int(dx/interval))].value for run in runs]) for dx in x]
                            mx = max([np.max([run.data[min(len(run.data)-1,int(dx/interval))].value for run in runs]) for dx in x])
                            mn = min([np.min([run.data[min(len(run.data)-1,int(dx/interval))].value for run in runs]) for dx in x])
                            quar1 = [max(mn,av-s) for av,s in zip(mean,std)]
                            quar2 = [min(mx,av+s) for av,s in zip(mean,std)]
                            quar1 = self.smoothRun(quar1,x)
                            quar2 = self.smoothRun(quar2,x)
                        
                            plt.fill_between(nx,quar1,quar2,color =col,alpha = 0.2)
                    colour += colourInc
                    plotIt += 1

            self.nPlots = plotIt

        plt.title(var if self.figOptions.titleVar.get() == "" else self.figOptions.titleVar.get())
        plt.xlabel(self.timeName if self.figOptions.xLabVar.get() == "" else self.figOptions.xLabVar.get())
        plt.ylabel("" if self.figOptions.yLabVar.get() == "" else self.figOptions.yLabVar.get())

        plt.grid(True)

        if self.figOptions.xMin.get() != "" and self.figOptions.xMax.get() != "":
            plt.xlim([float(self.figOptions.xMin.get()),float(self.figOptions.xMax.get())])
            plt.ticklabel_format(axis="x",style="sci", scilimits=(0,0))
        if self.figOptions.yMin.get() != "" and self.figOptions.yMax.get() != "":
            plt.ylim([float(self.figOptions.yMin.get()),float(self.figOptions.yMax.get())])

        if self.showLegend:
            if self.outLegend:

                plt.subplots_adjust(right=0.7)
                plt.legend(loc='center left', bbox_to_anchor=(1.04,0.5))
            else:
                plt.legend()

        plt.savefig("tmp.jpg")

        tmpImg = Image.open("tmp.jpg")
        tmpImg = tmpImg.convert("RGBA")
        self.currentImage = ImageTk.PhotoImage(tmpImg)
        self.canvas.configure(image = self.currentImage)
        self.canvas.image = self.currentImage
        os.remove("tmp.jpg")
        plt.clf()
        




    

if __name__ == "__main__":
    path = arg("--path",'./')
    name = arg("--fileName",'env_info')
    try:
        InteractiveWindow(path,name)
    except Exception as e:
        print("-----\nInteractive analysis of a set of data\n\nInteractiveAnalysis --path PATH --fileName FILENAME\n")
        print(f"Error: {e}")

    


        
