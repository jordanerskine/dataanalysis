import sys


def arg(tag, default):
    if tag in sys.argv:
        val = sys.argv[sys.argv.index(tag)+1]
    else:
        val = default
    return val

class DataPoint:
    def __init__(self,value,timestep):
        self.value = value
        self.timestep = timestep

class Run:
    def __init__(self,variant, name):
        self.variant = variant
        self.name = name
        self.data = []
    
    def listValues(self):
        return [d.value for d in self.data]

    def listTimes(self):
        return [d.timestep for d in self.data]


def fixNulls(s):
    for line in s:
        yield line.replace('\0',' ')